from typing import NamedTuple, Set


class Point(NamedTuple):
    x: int
    y: int


class Rope(NamedTuple):
    points: list[Point]


def move_head(current_head: Point, direction: str) -> Point:
    match direction:
        case "L":
            return Point(current_head.x - 1, current_head.y)
        case "R":
            return Point(current_head.x + 1, current_head.y)
        case "U":
            return Point(current_head.x, current_head.y + 1)
        case "D":
            return Point(current_head.x, current_head.y - 1)


def move_next(head_position: Point, tail_position: Point) -> Point:
    x_dist = head_position.x - tail_position.x
    y_dist = head_position.y - tail_position.y

    if abs(x_dist) <= 1 and abs(y_dist) <= 1:
        return tail_position

    if abs(x_dist) == 2:
        return Point(tail_position.x + x_dist // abs(x_dist), head_position.y)

    return Point(head_position.x, tail_position.y + y_dist // abs(y_dist))


def move_rope(rope: Rope, direction: str) -> Rope:
    old_rope = rope.points.copy()
    head = old_rope.pop(0)
    new_head = move_head(head, direction)
    new_rope: list[Point] = [new_head]
    for i in range(9):
        next_head = new_rope[-1]
        next_tail = move_next(next_head, old_rope.pop(0))
        new_rope.append(next_tail)
    assert len(old_rope) == 0
    return Rope(new_rope)


def main():
    visited: Set[Point] = set()
    with open("./aoc9.txt") as puzzle:
        lines = puzzle.readlines()

        points = [Point(0, 0) for _ in range(10)]
        visited.add(points[-1])
        rope = Rope(points)
        for line in lines:
            direction, count_str = line.split()
            count = int(count_str)
            for i in range(count):
                rope = move_rope(rope, direction)
                visited.add(rope.points[-1])

    print(len(visited))


if __name__ == '__main__':
    main()
