from typing import NamedTuple, Set


class Point(NamedTuple):
    x: int
    y: int


def move_head(current_head: Point, direction: str) -> Point:
    match direction:
        case "L":
            return Point(current_head.x - 1, current_head.y)
        case "R":
            return Point(current_head.x + 1, current_head.y)
        case "U":
            return Point(current_head.x, current_head.y + 1)
        case "D":
            return Point(current_head.x, current_head.y - 1)


def move_tail(head_position: Point, tail_position: Point) -> Point:
    x_dist = head_position.x - tail_position.x
    y_dist = head_position.y - tail_position.y

    if abs(x_dist) <= 1 and abs(y_dist) <= 1:
        return tail_position

    if abs(x_dist) == 2:
        return Point(tail_position.x + x_dist // abs(x_dist), head_position.y)

    return Point(head_position.x, tail_position.y + y_dist // abs(y_dist))


def main():
    visited: Set[Point] = set()

    with open("./aoc9.txt") as puzzle:
        lines = puzzle.readlines()
        current_head = Point(0, 0)
        current_tail = Point(0, 0)
        visited.add(current_tail)
        for line in lines:
            direction, count_str = line.split()
            count = int(count_str)
            for i in range(count):
                current_head = move_head(current_head, direction)
                current_tail = move_tail(current_head, current_tail)
                visited.add(current_tail)

    print(len(visited))


if __name__ == '__main__':
    main()
